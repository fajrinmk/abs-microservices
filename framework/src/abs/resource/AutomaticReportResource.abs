module MAutomaticReportResource;

import Income, IncomeImpl from MIncomeModel;
import IncomeDb, IncomeDbImpl from MIncomeDbImpl;
import Expense, ExpenseImpl from MExpenseModel;
import ExpenseDb, ExpenseDbImpl from MExpenseDbImpl;
import ChartOfAccount, ChartOfAccountImpl from MChartOfAccountModel;
import ChartOfAccountDb, ChartOfAccountDbImpl from MChartOfAccountDbImpl;
import ChartOfAccountEntry, ChartOfAccountEntryImpl from MChartOfAccountEntryModel;
import ChartOfAccountEntryDb, ChartOfAccountEntryDbImpl from MChartOfAccountEntryDbImpl;
import ABSHttpRequest from ABS.Framework.Http;
import Utility, UtilityImpl from ABS.Framework.Utility;

interface AutomaticReportResource {
    List<Income> getIncomes(ABSHttpRequest request);
    List<Expense> getExpenses(ABSHttpRequest request);
    List<ChartOfAccountEntry> filterByCodeLevelHelper(List<ChartOfAccountEntry> coaList, Int maxLevelLength);
    List<ChartOfAccountEntry> filterByCodeLevel(List<ChartOfAccountEntry> coaList, String prefixId, Int maxLevelLength);
    List<ChartOfAccountEntry> transFormToChartOfAccount(List<Income> incomes, List<Expense> expenses, Int codeLength);
    List<ChartOfAccountEntry> addDetail(List<ChartOfAccountEntry> coaSheets);
}

class AutomaticReportResourceImpl implements AutomaticReportResource {
    List<Income> getIncomes(ABSHttpRequest request) {
        IncomeDb orm = new local IncomeDbImpl();
        List<Income> incomes = orm.findAll("MIncomeModel.IncomeImpl_c");
        return incomes;
    }

    List<Expense> getExpenses(ABSHttpRequest request) {
        ExpenseDb expense_orm = new local ExpenseDbImpl();
        List<Expense> expenses = expense_orm.findAll("MExpenseModel.ExpenseImpl_c");
        return expenses;
    }

    List<ChartOfAccountEntry> transFormToChartOfAccount(List<Income> incomes, List<Expense> expenses, Int codeLength) {
        Map<String, ChartOfAccountEntry> entries = map[];

        Int count = 0;
        while(count < length(incomes)) {
            Income income = nth(incomes, count);
            String idCoa = income.getIdCoa();
            String code = substr(idCoa, 0, codeLength);
            Maybe<ChartOfAccountEntry> maybeEn = lookup(entries, code);

            if(isJust(maybeEn)) {
                ChartOfAccountEntry entry = fromJust(maybeEn);

                Int entryAmount = entry.getAmount();
                Int incomeAmount = income.getAmount();
                Int total = entryAmount + incomeAmount;

                entry.setAmount(total);
            } else {
                Utility utility = new local UtilityImpl();
                ChartOfAccountEntry newEntry = new local ChartOfAccountEntryImpl();

                Int newAmount = income.getAmount();
                newEntry.setAmount(newAmount);
                Int codeInt = utility.stringToInteger(code);
                newEntry.setIdChartOfAccount(codeInt);
                newEntry.setLevel(codeLength);

                entries = insert(entries, Pair(code, newEntry));
            }
            count = count + 1;
        }

        count = 0;
        while(count < length(expenses)) {
            Expense expense = nth(expenses, count);
            String idCoa = expense.getIdCoa();
            String code = substr(idCoa, 0, codeLength);
            Maybe<ChartOfAccountEntry> maybeEn = lookup(entries, code);

            if(isJust(maybeEn)) {
                ChartOfAccountEntry entry = fromJust(maybeEn);
                Int entryAmount = entry.getAmount();
                Int expenseAmount = expense.getAmount();
                Int total = entryAmount + expenseAmount;

                entry.setAmount(total);
            } else {
                Utility utility = new local UtilityImpl();
                ChartOfAccountEntry newEntry = new local ChartOfAccountEntryImpl();

                Int newAmount = expense.getAmount();
                newEntry.setAmount(newAmount);
                Int codeInt = utility.stringToInteger(code);
                newEntry.setIdChartOfAccount(codeInt);
                newEntry.setLevel(codeLength);

                entries = insert(entries, Pair(code, newEntry));
            }
            count = count + 1;
        }
        List<ChartOfAccountEntry> entryList = values(entries);

        return entryList;
    }

    List<ChartOfAccountEntry> addDetail(List<ChartOfAccountEntry> coaSheets) {
        Int count = 0;
        Int length = length(coaSheets);
        while(count < length) {
            Utility utility = new local UtilityImpl();
            ChartOfAccountEntry entry = nth(coaSheets, count);

            Int idInt = entry.getIdChartOfAccount();
            String id = toString(idInt);
            id = id + "0000";
            id = substr(id, 0, 5);
            String condition = "id=" + id;

            ChartOfAccountDb orm = new local ChartOfAccountDbImpl();
            ChartOfAccount chartOfAccount = orm.findByAttributes("MChartOfAccountModel.ChartOfAccountImpl_c",condition);

            String description = chartOfAccount.getDescription();
            entry.setDescription(description);
            String name = chartOfAccount.getName();
            entry.setName(name);
            Int code = utility.stringToInteger(id);
            entry.setIdChartOfAccount(code);

            count = count + 1;
        }

        return coaSheets;
    }

    List<ChartOfAccountEntry> filterByCodeLevelHelper(List<ChartOfAccountEntry> coaList, Int maxLevelLength) {
        List<ChartOfAccountEntry> coaSheetsFinal = list[];

        Int count = 1;
        while(count <= 6) {
            String countStr = toString(count);
            List<ChartOfAccountEntry> coaSheetsTmp = this.filterByCodeLevel(coaList, countStr, maxLevelLength);
            coaSheetsFinal = concatenate(coaSheetsFinal, coaSheetsTmp);
            count = count + 1;
        }

        return coaSheetsFinal;
    }

    List<ChartOfAccountEntry> filterByCodeLevel(List<ChartOfAccountEntry> coaList, String prefixId, Int maxLevelLength) {
        List<ChartOfAccountEntry> entries = list[];
        Int maxDigitIndex = strlen(prefixId);
        Int count = 0;

        while(count < length(coaList)) {
            ChartOfAccountEntry entry = nth(coaList, count);

            Int entryLevel = entry.getLevel();

            Int codeInt = entry.getIdChartOfAccount();
            String codeStr = toString(codeInt);
            String codeSubstr = substr(codeStr, 0, maxDigitIndex);
            Int childDigitLength = maxDigitIndex + 2;

            if(codeSubstr == prefixId) {
                if (entryLevel == maxDigitIndex) {
                    entries = appendright(entries, entry);
                } else if ((entryLevel <= childDigitLength) && (childDigitLength <= maxLevelLength)) {
                    String childLevelCode = substr(codeStr, 0, childDigitLength);
                    List<ChartOfAccountEntry> childEntries = this.filterByCodeLevel(coaList, childLevelCode, maxLevelLength);
                    entries = concatenate(entries, childEntries);
                }
            }

            count = count + 1;
        }

        return entries;
    }
}
