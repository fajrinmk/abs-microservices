# NON TECHNICAL

* Microservice berperan sebagai penyedia API,
nanti fronend akan nembak API ini untuk mendapatkan data.

* Directory structure:
  * framework: implementasi business logic
  * middleware: segala hal yg berkaitan dengan auth (facebook auth, google auth)

### Flow 
![Microservice Message Flow](docs/microservice-arch.png)

# TECHNICAL
### Build product 

pergi ke directory "<root>/framework", execute "bash build.sh"

  * output: NamaProduk.jar, disini mengandung semua library yg dibutuhkan, termasuk middleware.jar

### Build middleware

pergi ke directory "<root>/middleware", execute "ant"

  * output "<root>/middleware/dist/middleware.jar"

### Run product
> java -jar <NamaProduk.jar> -p <port,any>
> untuk akses, pergi ke http://localhost:[port], trus akses lewat route nya (lihat file route)

# Other

AbsDbOrm.jar => dihasilkan dari AbsDbOrm.java (ada di absmvc)
  - untuk dapetinnya coba clone absmvc, build disitu, copy lib/AbsDbOrm.jar nya

